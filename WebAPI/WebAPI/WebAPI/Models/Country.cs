﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Country
    {
        public int ID { get; set; }

        public string Title { get; set; }

    }
     
}
