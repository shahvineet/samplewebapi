﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    [DataContract(Name = "APIResponseModel")]
    public class APIResponseModel<T>
    {
        public APIResponseModel()
        {
            statusCode = (int)APIStatusCodes.NO_RESPONSE;
            statusMessage = APIStatusMessages.NO_RESPONSE;
        }
        [DataMember]
        public string statusMessage { set; get; }
        [DataMember]
        public int statusCode { set; get; }
        [DataMember]
        public int totalCount { set; get; }
        [DataMember]
        public string token { set; get; }

        private T _value;
        [DataMember]
        public T data
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }
    }
    public enum APIStatusCodes
    {
        //API Status Codes
        NO_RESPONSE = 10000,
        SUCCESS = 10001,
        WARNING = 10002,
        ERROR = 10003,
        NO_RECORD_FOUND = 10004,
        DATABASE_ERROR = 10005,
        INVALID_REQUEST = 10006,
        REQUIRED_DATA_MISSING = 10007,
        RECORD_CREATED = 10008,
        ALREADY_EXIST = 10009,
        SP_ERROR = 10010,
        UNABLE_TO_GENERATE_TOKEN = 10011,
        USER_NOT_EXIST = 10012,
        FAILURE = 10013,
        INVALID_VENDOR_SECRET_KEY = 5002,
        LICENSE_NOT_ACTIVE = 5003,
        LICENSE_EXPIRED = 5004,
        BOOKING_NOT_DONE = 60001,
        INVALID_JWT_TOKEN = 10014,
        UNABLE_TO_VALIDATE_TOKEN = 10015,

        APPROVED = 10016,
        DECLINED = 10017,
        PARTIAL_APPROVED = 10018,
        PAYMENT_ERROR = 10019,

        EMAIL_NOT_FOUND = 10020,
        EMAIL_ALREADY_EXIST = 10021,
    }
    public class APIStatusMessages
    {
        //NDB - This should come from Resource file
        //Constant Variables
        public const string NO_RESPONSE = "No response received.";
        public const string LIST_SUCCESS = "Data received successfully.";
        public const string NO_RECORDS_FOUND = "No records found.";

        public const string DATABASE_ERROR = "Database error.";
        public const string APPLICATION_ERROR = "Application error, please try again later.";
        public const string INVALID_REQUEST = "Invalid request, Please correct the data and try again.";
        public const string REQUIRED_DATA_MISSING = "Required Data is missing, Please check request data and try again.";
        public const string UNABLE_TO_GENERATE_TOKEN = "Unable to generate token, please try again.";
        public const string UNABLE_TO_VALIDATE_TOKEN = "Unable to validate token, please try again.";
        public const string TOKEN_EXPIRED = "Token expired, please try again.";
        public const string INVALID_JWT_TOKEN = "Invalid JWT Token.";
        public const string INVALID_EMAIL = "Invalid Email.";
        //User role management
        public const string RECORD_CREATED = "User created successfully.";
        public const string ALREADY_EXIST = "User already exist!";
        public const string DB_ERROR = "Database error in stored procedure.";
        public const string LOGIN_SUCCESS = "User logged in successfully.";
        public const string USER_NOT_EXIST = "Username/password is incorrect or user does not exist or inactive!";
        public const string INVALID_VENDOR_SECRET_KEY = "Incorrect vendor id or secrect key!";
        public const string LICENSE_NOT_ACTIVE = "License is inactive.";
        public const string LICENSE_EXPIRED = "License is expired.";

        //GLS
        public const string BOOKING_SUCCESS = "Booking received successfully.";
        public const string BOOKING_ERROR = "Unable to register booking, please try again.";

        public const string BOOKING_EMPTY = "Booking details can't be null or empty!";
        public const string RATETABLE_EMPTY = "Rate table can't be null or empty!";
        public const string LEADSAVED_SUCCESS = "New Lead Saved successfully.";
        public const string SUITSAVED_SUCCESS = "New Suit Saved successfully.";
        public const string AMENITY_UPDATE_SUCCESS = "Amenity updated successfully.";
        public const string SUITUPDATE_SUCCESS = "Suit Updated successfully.";
        public const string INVALID_SUIT = "Please provide a valid suit/building.";
        public const string NOTES_UPDATE_SUCCESS = "Notes updated successfully.";

        //GLS
        public const string TOKEN_SUCCESS = "Token parsed successfully.";

        //payment 
        //public const string General_ERROR = "Unable to register booking, please try again.";
        //public const string BOOKING_ERROR = "Unable to register booking, please try again.";

        //Referral
        public const string EMAIL_ALREADY_EXIST = "Email ALREADY EXIST";
        public const string EMAIL_NOT_FOUND = "Email NOT FOUND";

        //Integrations
        public const string SAVE_CANCELLATION_SUCCESS = "Booking cancelled successfully.";
        public const string SAVE_CANCELLATION_ERROR = "Unable to cancel booking, please try again.";
        public const string SAVE__SUCCESS = "Booking cancelled successfully.";
        public const string RATES_PROCESSED__SUCCESS = "Rates processed successfully.";
        public const string AVAILABILITY_PROCESSED__SUCCESS = "Availability processed successfully.";
    }
}
