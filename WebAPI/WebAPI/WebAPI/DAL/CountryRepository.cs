﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Helpers;
using WebAPI.Models;

namespace WebAPI.DAL
{
    public class CountryRepository
    {
        private readonly IOptions<ConnectionString> connectionString;

        public CountryRepository(IOptions<ConnectionString> _connectionString)
        {
            connectionString = _connectionString;
        }
        public APIResponseModel<List<Country>> Get(APIResponseModel<List<Country>> _objAPIResponseModel)
        {
            //Object creation
            List<Country> lstCountry = new List<Country>();
            try
            {
                //using (SqlConnection connection = new SqlConnection("Server=churchill99;DataBase=CCSMRI_Dev;user=API_User;password=ZehSakana!;"))
                using (SqlConnection connection = new SqlConnection(connectionString.Value.ConnectionStringAPI))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("dbo.API_GetCountries", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            //Check for records 
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    Country objCountry = new Country();
                                    objCountry.ID = Utility.DbInt(reader, "ID");
                                    objCountry.Title = Utility.DbString(reader, "Country").ToString();

                                    lstCountry.Add(objCountry);
                                }

                                //Preparing response 
                                _objAPIResponseModel.data = lstCountry;
                                _objAPIResponseModel.totalCount = 1;
                                _objAPIResponseModel.statusCode = (int)APIStatusCodes.SUCCESS;
                                _objAPIResponseModel.statusMessage = APIStatusMessages.LIST_SUCCESS;
                            }
                            else
                            {
                                //Preparing response 
                                _objAPIResponseModel.data = null;
                                _objAPIResponseModel.statusCode = (int)APIStatusCodes.NO_RECORD_FOUND;
                                _objAPIResponseModel.statusMessage = APIStatusMessages.NO_RECORDS_FOUND;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _objAPIResponseModel.data = null;
                _objAPIResponseModel.statusCode = (int)APIStatusCodes.DATABASE_ERROR;
                _objAPIResponseModel.statusMessage = APIStatusMessages.DATABASE_ERROR;
            }
            finally
            {
                lstCountry = null;
            }
            //Returing the values.
            return _objAPIResponseModel;
        }
    }
}
