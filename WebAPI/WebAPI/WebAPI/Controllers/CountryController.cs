﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using WebAPI.DAL;
using WebAPI.Helpers;
using WebAPI.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class CountryController : Controller
    {
        private readonly IOptions<ConnectionString> connectionString;

        public CountryController(IOptions<ConnectionString> _connectionString)
        {
            connectionString = _connectionString;
        }

        //// GET: api/values
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            //Object creation
            APIResponseModel<List<Country>> _objAPIResponseModel = new APIResponseModel<List<Country>>();
            CountryRepository _objCountryRepository = new CountryRepository(connectionString);

            //Calling method to get the data.
            _objAPIResponseModel = _objCountryRepository.Get(_objAPIResponseModel);
            return Utility.ConvertToHttpResponse(Request, _objAPIResponseModel);

        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
