﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Helpers
{
    public static class Utility
    {
        public static DateTime DbDateTime(IDataReader reader, string FieldName)
        {
            int ord = reader.GetOrdinal(FieldName);
            return reader.IsDBNull(ord) ? DateTime.Now : reader.GetDateTime(ord);
        }

        public static string FormatDateTime(DateTime dateTime)
        {
            return String.Format("{0:s}Z", dateTime);
        }

        public static string DbNull(object o)
        {
            if (o == DBNull.Value)
                return "";
            else
                return (string)o;
        }

        public static string DbString(IDataReader reader, string FieldName)
        {
            int ord = reader.GetOrdinal(FieldName);
            return reader.IsDBNull(ord) ? "" : reader.GetString(ord).Trim();
        }

        public static int DbInt(IDataReader reader, string FieldName)
        {
            int ord = reader.GetOrdinal(FieldName);
            return reader.IsDBNull(ord) ? 0 : reader.GetInt32(ord);
        }


        public static bool DbBool(IDataReader reader, string FieldName)
        {
            int ord = reader.GetOrdinal(FieldName);
            return reader.IsDBNull(ord) ? false : reader.GetBoolean(ord);
        }

        public static double DbDouble(IDataReader reader, string FieldName)
        {
            int ord = reader.GetOrdinal(FieldName);
            return reader.IsDBNull(ord) ? 0 : reader.GetDouble(ord);
        }

        public static decimal DbDecimal(IDataReader reader, string FieldName)
        {
            int ord = reader.GetOrdinal(FieldName);
            return reader.IsDBNull(ord) ? 0 : reader.GetDecimal(ord);
        }

        public static string DbDecimalToString(IDataReader reader, string FieldName)
        {
            int ord = reader.GetOrdinal(FieldName);
            return reader.IsDBNull(ord) ? "" : (DbDecimal(reader, FieldName) == 0 ? "" : DbDecimal(reader, FieldName).ToString());
        }

        public static string DbIntToString(IDataReader reader, string FieldName)
        {
            int ord = reader.GetOrdinal(FieldName);
            return reader.IsDBNull(ord) ? "" : (DbInt(reader, FieldName) == 0 ? "" : DbInt(reader, FieldName).ToString());
        }

        public static DateTime DbDateTimeMin(IDataReader reader, string FieldName)
        {
            int ord = reader.GetOrdinal(FieldName);
            return reader.IsDBNull(ord) ? DateTime.MinValue : reader.GetDateTime(ord);
        }

        public static Nullable<DateTime> DbNDateTime(IDataReader reader, string FieldName)
        {
            int ord = reader.GetOrdinal(FieldName);
            return reader.IsDBNull(ord) ? (Nullable<DateTime>)null : reader.GetDateTime(ord);
        }

        public static Nullable<int> DBNInt(IDataReader reader, string FieldName)
        {
            int ord = reader.GetOrdinal(FieldName);
            if (reader.IsDBNull(ord))
            { return null; }
            else
            {
                return reader.GetInt32(ord);
            }
        }

        public static Nullable<DateTime> NdtNullableDateTime(string strDate)
        {
            DateTime dtNullableDateTime;
            if (DateTime.TryParse(strDate, out dtNullableDateTime))
            {
                return dtNullableDateTime;
            }
            else
            {
                return null;
            }
        }

        public static Nullable<int> nParseInt(string strInt)
        {
            int nInt;
            if (int.TryParse(strInt, out nInt))
            {
                return nInt;
            }
            else
            {
                return null;
            }
        }

        public static Nullable<float> fParseFloat(string strFloat)
        {
            float fFloat;
            if (float.TryParse(strFloat, out fFloat))
            {
                return fFloat;
            }
            else
            {
                return null;
            }
        }

        public static bool bIsNotNullEmpty(string strValue)
        {
            if (strValue == null || strValue == "")
                return false;
            else
                return true;
        }

        public static decimal dDecimal(string strDecimal)
        {
            if (strDecimal == "")
            {
                return 0;
            }
            else
            {
                return decimal.Parse(strDecimal);
            }
        }

        public static int nInteger(string strInt)
        {
            if (strInt == "")
                return 0;
            else
                return int.Parse(strInt);
        }


        //public static Nullable<T> ToNullable<T>(string s) where T : struct
        //{
        //    Nullable<T> result = new Nullable<T>();
        //    if (!string.IsNullOrEmpty(s))
        //    {
        //        TypeConverter conv = TypeDescriptor.GetConverter(typeof(T));
        //        result = (T)conv.ConvertFrom(s);
        //    }

        //    return result;
        //}
        //no sunday for now
        public enum Days
        {
            Monday = 1, Tuesday = 2, Wednesday = 3, Thursday = 4, Friday = 5, Saturday = 6
        }

        public static T DataValue<T>(IDataReader reader, string FieldName)
        {
            int ord = reader.GetOrdinal(FieldName);
            return reader.IsDBNull(ord) ? default(T) : (T)reader[ord];
        }

        public static DateTime? TryParse(string text)
        {
            DateTime date;
            return DateTime.TryParse(text, out date) ? date : (DateTime?)null;
        }

        /// <summary>
        /// This method will be used to convert Application response to standard HTTP Response.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="httpRequest"></param>
        /// <param name="_objAPIResponseModel"></param>
        /// <param name="strToken"></param>
        /// <returns></returns>
        public static IActionResult ConvertToHttpResponse<T>(HttpRequest httpRequest, APIResponseModel<T> _objAPIResponseModel, string strToken = "")
        {
            if (_objAPIResponseModel.statusCode == (int)APIStatusCodes.DATABASE_ERROR)
                return new BadRequestObjectResult(_objAPIResponseModel);
            else if (_objAPIResponseModel.statusCode == (int)APIStatusCodes.SP_ERROR)
                return new BadRequestObjectResult(_objAPIResponseModel);
            else if (_objAPIResponseModel.statusCode == (int)APIStatusCodes.NO_RESPONSE)
                return new BadRequestObjectResult(_objAPIResponseModel);
            else if (_objAPIResponseModel.statusCode == (int)APIStatusCodes.INVALID_REQUEST)
                return new BadRequestObjectResult(_objAPIResponseModel);
            else if (_objAPIResponseModel.statusCode == (int)APIStatusCodes.REQUIRED_DATA_MISSING)
                return new BadRequestObjectResult(_objAPIResponseModel);
            else // NO_RECORD_FOUND/SUCCESS/WARNING/ERROR/RECORD_CREATED/ALREADY_EXIST/UNABLE_TO_GENERATE_TOKEN
                return new OkObjectResult(_objAPIResponseModel);
        }
    }
}
